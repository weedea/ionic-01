# README #

Ionic framework introduction: 
* Docker & environment 
* MyFirstApp 
* Scaffolding 
* Layouts


### How do I get set up? ###

* Git clone
* Git checkout docker branch
* Create the image from Dockerfile

<code>
docker build *docker-files-path* --tag=*prject_name*
</code> 
<br/>
<code>
docker run --network=host -it --privileged -v $PWD/:/app --name=*prject_name* *image_name* bash -l
</code>
<br/>
<br/>

* Create and start first app

<code>
ionic start app tabs --type=angular
</code>
 
 <br/>
 <code>
 cd app/
 </code>
<br/>
 <code>
 ionic serve
 </code>
<br/>
 

