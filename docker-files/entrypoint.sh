#!/bin/bash

set -e 

if [ ! -f "/app/angular.json" ]; then
  ionic start app tabs --type=angular --no-git 
fi

cd /app

if [ ! -d "/app/node_modules" ]; then
  npm install
fi

exec ionic serve --port 8100 --address=0.0.0.0
